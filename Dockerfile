# to create, and upload this machine
# cd <here>
# docker build -t thsfewer/arm-dev .
# docker push thsfewer/arm-dev 

FROM armv7/armhf-ubuntu

RUN apt-get update -y && apt-get install wget bzip2 build-essential libgtk2.0-dev -y
RUN wget https://storage.googleapis.com/golang/go1.7.1.linux-armv6l.tar.gz
RUN tar -xvf go1.7.1.linux-armv6l.tar.gz
RUN mv go /usr/local

RUN mkdir $HOME/go
RUN echo "export GOROOT=/usr/local/go" >> $HOME/.bashrc
RUN echo "export GOPATH=$HOME/go" >> $HOME/.bash
RUN echo "export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> $HOME/.bashrc
